﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Messaging;

namespace MSMQ
{
    public partial class frmMain : Form
    {
        private MessageQueue q = null;          // очередь сообщений
        private MessageQueue qL = null;          // очередь сообщений login
        private Thread t = null;                // поток, отвечающий за работу с очередью сообщений
        private Thread tL = null;                // поток, отвечающий за работу с очередью login
        private bool _continue = true;          // флаг, указывающий продолжается ли работа с мэйлслотом

        private Dictionary<string, MessageQueue> clientQueues = new Dictionary<string, MessageQueue>();

        // конструктор формы
        public frmMain()
        {
            InitializeComponent();
            string path = Dns.GetHostName() + "\\private$\\ServerQueue";    // путь к очереди сообщений, Dns.GetHostName() - метод, возвращающий имя текущей машины

            // если очередь сообщений с указанным путем существует, то открываем ее, иначе создаем новую
            if (MessageQueue.Exists(path))
                q = new MessageQueue(path);
            else
                q = MessageQueue.Create(path);

            string pathL = Dns.GetHostName() + "\\private$\\ServerQueueLogin";
            // если очередь сообщений с указанным путем существует, то открываем ее, иначе создаем новую
            if (MessageQueue.Exists(pathL))
                qL = new MessageQueue(pathL);
            else
                qL = MessageQueue.Create(pathL);

            // задаем форматтер сообщений в очереди
            q.Formatter = new XmlMessageFormatter(new Type[] { typeof(String) });
            qL.Formatter = new XmlMessageFormatter(new Type[] { typeof(String) });

            // вывод пути к очереди сообщений в заголовок формы, чтобы можно было его использовать для ввода имени в форме клиента, запущенного на другом вычислительном узле
            this.Text += "     " + q.Path;

            // создание потока, отвечающего за работу с очередью сообщений
            t = new Thread(ReceiveMessage);
            t.Start();
            tL = new Thread(ReceiveLogin);
            tL.Start();
        }
        private void ReceiveLogin()
        {
            if (qL == null)
                return;

            System.Messaging.Message msg = null;

            // входим в бесконечный цикл работы с очередью сообщений
            while (_continue)
            {
                if (qL.Peek() != null)   // если в очереди есть сообщение, выполняем его чтение, интервал до следующей попытки чтения равен 10 секундам
                    msg = qL.Receive(TimeSpan.FromSeconds(10.0));

                string clientPath = msg.Body.ToString();
                if (MessageQueue.Exists(clientPath))
                {
                    clientQueues[msg.Label] = new MessageQueue(clientPath);
                }
                else
                {
                    rtbMessages.Invoke((MethodInvoker)delegate
                    {
                        rtbMessages.Text += "\n >> Клиент подключился без принимающей очереди!";
                    });
                }
            };
            Thread.Sleep(500);          // приостанавливаем работу потока перед тем, как приcтупить к обслуживанию очередного клиента
        }

        // получение сообщения
        private void ReceiveMessage()
        {
            if (q == null)
                return;

            System.Messaging.Message msg = null;

            // входим в бесконечный цикл работы с очередью сообщений
            while (_continue)
            {
                if (q.Peek() != null)   // если в очереди есть сообщение, выполняем его чтение, интервал до следующей попытки чтения равен 10 секундам
                    msg = q.Receive(TimeSpan.FromSeconds(10.0));

                rtbMessages.Invoke((MethodInvoker)delegate
                {
                    if (msg != null)
                    {
                        rtbMessages.Text += "\n >> " + msg.Label + " : " + msg.Body;     // выводим полученное сообщение на форму                        
                        foreach (string client in clientQueues.Keys)
                        {
                            if (MessageQueue.Exists(clientQueues[client].Path))
                            {
                                clientQueues[client].Send(msg.Body, msg.Label);
                            }
                            else
                            {
                                clientQueues.Remove(client);
                            }
                        }
                    }
                });
                Thread.Sleep(500);          // приостанавливаем работу потока перед тем, как приcтупить к обслуживанию очередного клиента
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            _continue = false;      // сообщаем, что работа с очередью сообщений завершена

            if (t != null)
            {
                t.Abort();          // завершаем поток
            }
            if (tL != null)
            {
                tL.Abort();          // завершаем поток
            }

            if (q != null)
            {
                MessageQueue.Delete(q.Path);      // в случае необходимости удаляем очередь сообщений
            }

            if (qL != null)
            {
                MessageQueue.Delete(qL.Path);      // в случае необходимости удаляем очередь сообщений
            }
        }
    }
}