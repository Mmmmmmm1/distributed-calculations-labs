﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Messaging;
using System.Threading;

namespace MSMQ
{
    public partial class frmMain : Form
    {
        private MessageQueue q = null;      // очередь сообщений, в которую будет производиться запись сообщений
        private string login;
        private MessageQueue clientQ = null;
        private Thread t = null;                // поток, отвечающий за работу с очередью сообщений
        private bool _continue = true;          // флаг, указывающий продолжается ли работа с мэйлслотом

        // конструктор формы
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (MessageQueue.Exists(tbPath.Text))
            {               
                login = loginT.Text;
                string clientPath = Dns.GetHostName() + "\\private$\\" + login;

                if (MessageQueue.Exists(clientPath))
                    clientQ = new MessageQueue(clientPath);
                else
                    clientQ = MessageQueue.Create(clientPath);

                new MessageQueue(tbPath.Text + "Login").Send(clientPath, login);

                clientQ.Formatter = new XmlMessageFormatter(new Type[] { typeof(String) });

                // создание потока, отвечающего за работу с очередью сообщений
                t = new Thread(ReceiveMessage);
                t.Start();

 // если очередь, путь к которой указан в поле tbPath существует, то открываем ее
                q = new MessageQueue(tbPath.Text);
                btnSend.Enabled = true;
                btnConnect.Enabled = false;
            }
            else
                MessageBox.Show("Указан неверный путь к очереди, либо очередь не существует");
        }

        // получение сообщения
        private void ReceiveMessage()
        {
            if (clientQ == null)
                return;

            System.Messaging.Message msg = null;

            // входим в бесконечный цикл работы с очередью сообщений
            while (_continue)
            {
                if (clientQ.Peek() != null)   // если в очереди есть сообщение, выполняем его чтение, интервал до следующей попытки чтения равен 10 секундам
                    msg = clientQ.Receive(TimeSpan.FromSeconds(10.0));

                chat.Invoke((MethodInvoker)delegate
                {
                    if (msg != null)
                    {
                        chat.Text += "\n >> " + msg.Label + " : " + msg.Body;     // выводим полученное сообщение на форму                        
                        
                    }
                });
                Thread.Sleep(500);          // приостанавливаем работу потока перед тем, как приcтупить к обслуживанию очередного клиента
            }
        }


        private void btnSend_Click(object sender, EventArgs e)
        {
            // выполняем отправку сообщения в очередь
            q.Send(tbMessage.Text, login);
        }

        private void loginT_TextChanged(object sender, EventArgs e)
        {
            if (loginT.Text.Length == 0)
            {
                loginT.BackColor = Color.Pink;
                btnConnect.Enabled = false;
            }
            else
            {
                loginT.BackColor = Color.White;
                btnConnect.Enabled = true;
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {

            _continue = false;      // сообщаем, что работа с очередью сообщений завершена

            if (t != null)
            {
                t.Abort();          // завершаем поток
            }           

            if (clientQ != null)
            {
                MessageQueue.Delete(clientQ.Path);      // в случае необходимости удаляем очередь сообщений
            }
        }
    }
}