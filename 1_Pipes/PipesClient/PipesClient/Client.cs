﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace Pipes
{
    public partial class frmMain : Form
    {
        private Int32 LoginPipeHandle;   // дескриптор канала
        private Int32 ChatPipeHandle;   // дескриптор канала
        private Int32 PipeHandle;   // дескриптор канала
        private string login;
        private string userPipeName;
        private string serverPipeName;
        private string serverPipeNameLogin;
        private const string separator = " >> ";
        private Thread t;                                                               // поток для обслуживания канала
        private bool _continue = true;                                                  // флаг, указывающий продолжается ли работа с каналом

        // конструктор формы
        public frmMain()
        {
            InitializeComponent();
            //this.Text += "     " + Dns.GetHostName();   // выводим имя текущей машины в заголовок формы
            Login frm = new Login();
            DialogResult res = frm.ShowDialog();
            if (res == DialogResult.OK)
            {
                login = frm.loginText.Text;
                userPipeName = "\\\\.\\pipe\\" + login;
                serverPipeName = frm.tbPipe.Text;
                serverPipeNameLogin = serverPipeName + ".login";

                uint BytesWritten = 0;  // количество реально записанных в канал байт
                byte[] buff = Encoding.Unicode.GetBytes(login + separator + userPipeName + separator + "add" + separator);    // выполняем преобразование сообщения (вместе с идентификатором машины) в последовательность байт

                // открываем именованный канал, имя которого указано в поле tbPipe
                LoginPipeHandle = DIS.Import.CreateFile(serverPipeNameLogin, DIS.Types.EFileAccess.GenericWrite, DIS.Types.EFileShare.Read, 0, DIS.Types.ECreationDisposition.OpenExisting, 0, 0);
                DIS.Import.WriteFile(LoginPipeHandle, buff, Convert.ToUInt32(buff.Length), ref BytesWritten, 0);         // выполняем запись последовательности байт в канал
                DIS.Import.CloseHandle(LoginPipeHandle);
            }
            this.Text += "     " + login;   // выводим имя текущей машины в заголовок формы
            log.Text += login;
            serv.Text += serverPipeName;

            ChatPipeHandle = DIS.Import.CreateNamedPipe(userPipeName, DIS.Types.PIPE_ACCESS_DUPLEX, DIS.Types.PIPE_TYPE_BYTE | DIS.Types.PIPE_WAIT, DIS.Types.PIPE_UNLIMITED_INSTANCES, 0, 1024, DIS.Types.NMPWAIT_WAIT_FOREVER, (uint)0);
            // создание потока, отвечающего за работу с каналом
            t = new Thread(ReceiveMessage);
            t.Start();
        }
        private void ReceiveMessage()
        {
            string msg = "";            // прочитанное сообщение
            uint realBytesReaded = 0;   // количество реально прочитанных из канала байтов

            // входим в бесконечный цикл работы с каналом
            while (_continue)
            {
                if (DIS.Import.ConnectNamedPipe(ChatPipeHandle, 0))
                {
                    byte[] buff = new byte[1024];                                           // буфер прочитанных из канала байтов
                    DIS.Import.FlushFileBuffers(ChatPipeHandle);                                // "принудительная" запись данных, расположенные в буфере операционной системы, в файл именованного канала
                    DIS.Import.ReadFile(ChatPipeHandle, buff, 1024, ref realBytesReaded, 0);    // считываем последовательность байтов из канала в буфер buff
                    msg = Encoding.Unicode.GetString(buff);                                 // выполняем преобразование байтов в последовательность символов
                    chat.Invoke((MethodInvoker)delegate
                    {
                        if (msg != "")
                        {
                            chat.Text += " >> " + msg;                             // выводим полученное сообщение на форму
                            chat.Text += " \n";
                        }
                    });

                    DIS.Import.DisconnectNamedPipe(ChatPipeHandle);                             // отключаемся от канала клиента 
                    Thread.Sleep(500);                                                      // приостанавливаем работу потока перед тем, как приcтупить к обслуживанию очередного клиента
                }
            }
        }
        private void btnSend_Click(object sender, EventArgs e)
        {
            uint BytesWritten = 0;  // количество реально записанных в канал байт
            byte[] buff = Encoding.Unicode.GetBytes(login + separator + tbMessage.Text);    // выполняем преобразование сообщения (вместе с идентификатором машины) в последовательность байт

            // открываем именованный канал, имя которого указано в поле tbPipe
            PipeHandle = DIS.Import.CreateFile(serverPipeName, DIS.Types.EFileAccess.GenericWrite, DIS.Types.EFileShare.Read, 0, DIS.Types.ECreationDisposition.OpenExisting, 0, 0);
            DIS.Import.WriteFile(PipeHandle, buff, Convert.ToUInt32(buff.Length), ref BytesWritten, 0);         // выполняем запись последовательности байт в канал
            DIS.Import.CloseHandle(PipeHandle);                                                                 // закрываем дескриптор канала
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            uint BytesWritten = 0;  // количество реально записанных в канал байт
            byte[] buff = Encoding.Unicode.GetBytes(login + separator + userPipeName + separator + "del" + separator);    // выполняем преобразование сообщения (вместе с идентификатором машины) в последовательность байт

            // открываем именованный канал, имя которого указано в поле tbPipe
            LoginPipeHandle = DIS.Import.CreateFile(serverPipeNameLogin, DIS.Types.EFileAccess.GenericWrite, DIS.Types.EFileShare.Read, 0, DIS.Types.ECreationDisposition.OpenExisting, 0, 0);
            DIS.Import.WriteFile(LoginPipeHandle, buff, Convert.ToUInt32(buff.Length), ref BytesWritten, 0);         // выполняем запись последовательности байт в канал
            DIS.Import.CloseHandle(LoginPipeHandle);

            if (t != null)
                t.Abort();          // завершаем поток
            DIS.Import.CloseHandle(ChatPipeHandle);     // закрываем дескриптор канала
        }

    }
}
