﻿using Pipes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pipes
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void loginText_TextChanged(object sender, EventArgs e)
        {
            if (loginText.Text.Length == 0)
            {
                loginText.BackColor = Color.Pink;
                btnSend.Enabled = false;
            }
            else
            {
                loginText.BackColor = Color.White;
                btnSend.Enabled = true;
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
