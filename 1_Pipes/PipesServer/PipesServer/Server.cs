﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.Remoting.Messaging;

namespace Pipes
{
    public partial class frmMain : Form
    {
        private Int32 LoginPipeHandle;          // дескриптор канала логинов
        private Int32 PipeHandle;                                                  // дескриптор канала сообщений
        private Int32 UserPipeHandle;                                                  // дескриптор канала сообщений
        private string PipeName = "\\\\" + Dns.GetHostName() + "\\pipe\\ServerPipe";    // имя канала, Dns.GetHostName() - метод, возвращающий имя машины, на которой запущено приложение
        private Thread t;                                                               // поток для обслуживания канала
        private Thread lT;                                                              // поток для обслуживания логинов клиентов
        private bool _continue = true;                                                  // флаг, указывающий продолжается ли работа с каналом
        private Logins logins = new Logins();
        private const string separator = " >> ";

        // конструктор формы
        public frmMain()
        {
            InitializeComponent();

            // создание именованного канала
            PipeHandle = DIS.Import.CreateNamedPipe("\\\\.\\pipe\\ServerPipe", DIS.Types.PIPE_ACCESS_DUPLEX, DIS.Types.PIPE_TYPE_BYTE | DIS.Types.PIPE_WAIT, DIS.Types.PIPE_UNLIMITED_INSTANCES, 0, 1024, DIS.Types.NMPWAIT_WAIT_FOREVER, (uint)0);

            // создание канала работы с логинами
            LoginPipeHandle = DIS.Import.CreateNamedPipe("\\\\.\\pipe\\ServerPipe.login", DIS.Types.PIPE_ACCESS_DUPLEX, DIS.Types.PIPE_TYPE_BYTE | DIS.Types.PIPE_WAIT, DIS.Types.PIPE_UNLIMITED_INSTANCES, 0, 1024, DIS.Types.NMPWAIT_WAIT_FOREVER, (uint)0);

            // вывод имени канала в заголовок формы, чтобы можно было его использовать для ввода имени в форме клиента, запущенного на другом вычислительном узле
            this.Text += "     " + PipeName;

            // создание потока, отвечающего за работу с каналом
            t = new Thread(ReceiveMessage);
            t.Start();

            // создание потока, отвечающего за работу с логинами клиентов
            lT = new Thread(manageLogin);
            lT.Start();
        }

        private void manageLogin()
        {
            string msg = "";            // прочитанное сообщение
            uint realBytesReaded = 0;   // количество реально прочитанных из канала байтов
            while (_continue)
            {
                if (DIS.Import.ConnectNamedPipe(LoginPipeHandle, 0))
                {
                    byte[] buff = new byte[1024];                                           // буфер прочитанных из канала байтов
                    DIS.Import.FlushFileBuffers(LoginPipeHandle);                                // "принудительная" запись данных, расположенные в буфере операционной системы, в файл именованного канала
                    DIS.Import.ReadFile(LoginPipeHandle, buff, 1024, ref realBytesReaded, 0);    // считываем последовательность байтов из канала в буфер buff
                    msg = Encoding.Unicode.GetString(buff);                                 // выполняем преобразование байтов в последовательность символов
                    string[] msgs = msg.Split(new string[] { separator }, StringSplitOptions.None); //разделяем сообщение на части для выделения логина, pipe и действия

                    //msgs[0] - введеный логин клиента
                    //msgs[1] - имя pipe данного клиента
                    //msgs[2] - требуется добавить (add) или удалить (del) логин 
                    //msgs[3] - свободный буфер
                    switch (msgs[2])
                    {
                        case "add":
                            {
                                logins[msgs[0]] = msgs[1];
                            }
                            break;
                        case "del":
                            {
                                logins.Remove(msgs[0]);
                            }
                            break;
                    }


                    users.Invoke((MethodInvoker)delegate
                    {
                        users.Text = logins.ToString();                             // выводим все текущие логины
                    });

                    DIS.Import.DisconnectNamedPipe(LoginPipeHandle);                             // отключаемся от канала клиента 
                    Thread.Sleep(500);
                }
            }
        }

        private void ReceiveMessage()
        {
            string msg = "";            // прочитанное сообщение
            uint realBytesReaded = 0;   // количество реально прочитанных из канала байтов

            // входим в бесконечный цикл работы с каналом
            while (_continue)
            {
                if (DIS.Import.ConnectNamedPipe(PipeHandle, 0))
                {
                    byte[] buff = new byte[1024];                                           // буфер прочитанных из канала байтов
                    DIS.Import.FlushFileBuffers(PipeHandle);                                // "принудительная" запись данных, расположенные в буфере операционной системы, в файл именованного канала
                    DIS.Import.ReadFile(PipeHandle, buff, 1024, ref realBytesReaded, 0);    // считываем последовательность байтов из канала в буфер buff
                    msg = Encoding.Unicode.GetString(buff);                                 // выполняем преобразование байтов в последовательность символов
                    rtbMessages.Invoke((MethodInvoker)delegate
                    {
                        if (msg != "")
                        {
                            rtbMessages.Text += " >> " + msg;                             // выводим полученное сообщение на форму
                            rtbMessages.Text += " \n";
                        }
                    });
                    foreach (string login in logins.Keys)
                    {
                        uint BytesWritten = 0;  // количество реально записанных в канал байт
                        // открываем именованный канал, имя которого указано в поле tbPipe
                        UserPipeHandle = DIS.Import.CreateFile(logins[login], DIS.Types.EFileAccess.GenericWrite, DIS.Types.EFileShare.Read, 0, DIS.Types.ECreationDisposition.OpenExisting, 0, 0);
                        DIS.Import.WriteFile(UserPipeHandle, buff, Convert.ToUInt32(buff.Length), ref BytesWritten, 0);         // выполняем запись последовательности байт в канал
                        DIS.Import.CloseHandle(UserPipeHandle);                                                                 // закрываем дескриптор канала
                    }
                    DIS.Import.DisconnectNamedPipe(PipeHandle);                             // отключаемся от канала клиента 
                    Thread.Sleep(500);                                                      // приостанавливаем работу потока перед тем, как приcтупить к обслуживанию очередного клиента
                }
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            _continue = false;      // сообщаем, что работа с каналом завершена

            if (t != null)
                t.Abort();          // завершаем поток
            if (lT != null)
                lT.Abort();          // завершаем поток

            if (PipeHandle != -1)
                DIS.Import.CloseHandle(PipeHandle);     // закрываем дескриптор канала
        }
    }
    public class Logins : Dictionary<string, string>
    {
        public override string ToString()
        {
            string list = "";
            foreach (string login in this.Keys)
            {
                list += "\n" + login;
            }
            return list;
        }
    }
}