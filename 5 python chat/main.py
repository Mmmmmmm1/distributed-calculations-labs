import json
from flask import Flask, request, render_template
from datetime import datetime
from flask_cors import CORS, cross_origin

app = Flask(__name__, static_folder="./client", template_folder="./client")  # Настройки приложения
cors = CORS(app, methods=["GET", "POST"])

# function to add to JSON
def add_to_json(new_data, filename='history.json'):
    with open(filename,'r+') as file:
        json.dump(new_data, file, indent = 4)

f = open("history.json")
history = json.load(f)
f.close()

deleted_msgs = []

@app.route("/chat")
def chat_page():
    return render_template("chat.html")


def add_message(sender, text):
    new_message = {
        "sender": sender,
        "text": text,
        "time": datetime.now().strftime("%H:%M"),
        "msg_id": history["cur_id"]
    }
    history["cur_id"] += 1
    history["messages"].append(new_message)
    add_to_json(history)

def delete_message(id):
    deleted_msgs.append(id)
    history["messages"].remove(mes for mes in history["messages"] if mes["msg_id"] == id)
    add_to_json(history)

# API для получения списка сообщений

@app.route("/get_messages")
def get_messages():
    after_id = request.args["after"]
    res = [m for m in history["messages"] if m["msg_id"] > int(after_id)]
    return {"messages": res, "users":history["users"], "to_delete": deleted_msgs}

# HTTP-POST
@app.route("/login", methods=['POST'])
def login_user():
    user = request.args["name"]
    res = 0
    if user in history["users"]:
        res = history["users"][user]["first_msg_id"]
    else: 
        if len(history["messages"]) != 0:  
            res = history["messages"][-1]["msg_id"]
        history["users"][user] = {"first_msg_id": res}
        add_to_json(history)
    return {"first_message": res}

# HTTP-GET
# API для получения отправки сообщения  /send_message?sender=Mike&text=Hello
@app.route("/send_message")
def send_message():
    sender = request.args["sender"]
    text = request.args["text"]
    add_message(sender, text)
    return {"result": True}

@app.route("/delete_message")
def set_msg_deleted():
    id = request.args["id"]
    delete_message(id)
    return 

# Главная страница
@app.route("/")
def hello_page():
    return "New text goes here"


app.run()
