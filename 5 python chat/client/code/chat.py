from utils1 import utils as u
import js 

last_seen_id = 0
# Находим элементы интерфейса по их ID
send_message = js.document.getElementById("send_message")
send_login = js.document.getElementById("send_login")
sender = js.document.getElementById("sender")
message_text = js.document.getElementById("message_text")
chat_window = js.document.getElementById("chat_window")
user_window = js.document.getElementById("user_window")
serverURL="http://127.0.0.1:5000"
users = {}

async def delete_message_button(e):
    delete_message(e.target.id)
    await u.fetch(f"/delete_message?id={e.target.id}", method="GET")
    return

def delete_message(id):
    msg = js.document.getElementById(f'msg{id}')
    if msg:
        msg.remove()
    return

# Добавляет новое сообщение в список сообщений
def append_message(message):
    # Создаем HTML-элемент представляющий сообщение
    item = js.document.createElement("li")  # li - это HTML-тег для элемента списка
    item.id = f'msg{message["msg_id"]}'
    item.className = "list-group-item"   # className - определяет как элемент выглядит
    # Добавляем его в список сообщений (chat_window)
    if message["sender"] == sender.value:
        item.innerHTML = f'[<b>{message["sender"]}</b>]: <span>{message["text"]}</span><span class="badge text-bg-light text-secondary">{message["time"]}</span><button id="{message["msg_id"]}" class="btn btn-danger">delete</button>'
        chat_window.prepend(item)
        js.document.getElementById(f'{message["msg_id"]}').onclick = delete_message_button
    else:
        item.innerHTML = f'[<b>{message["sender"]}</b>]: <span>{message["text"]}</span><span class="badge text-bg-light text-secondary">{message["time"]}</span>'
        chat_window.prepend(item)

# Добавляет новое сообщение в список сообщений
def update_users(diff: []):
    for user in diff:
        # Создаем HTML-элемент представляющий сообщение
        item = js.document.createElement("li")  # li - это HTML-тег для элемента списка
        item.className = "list-group-item"   # className - определяет как элемент выглядит
        item.innerHTML = f'<b>{user}</b>'
        user_window.prepend(item)

def find_new_users(new_u: []):
    global users
    res = []
    for user in new_u:
        if user not in users:
            res.append(user)
    return res

# Вызывается при клике на send_message
async def send_message_click(e):
    # Отправляем запрос
    await u.fetch(f"/send_message?sender={sender.value}&text={message_text.value}", method="GET")
    # Очищаем поле
    message_text.value = ""

async def send_login_click(e):    
    if sender.value != "":
        sender.disabled = True
        send_message.style.visibility = "visible"
        send_login.style.visibility = "hidden"
        result = await u.fetch(f"/login?name={sender.value}", method="POST", payload = {})
        data = await result.json()
        global last_seen_id
        last_seen_id = data["first_message"]
        await load_fresh_messages()


# Загружает новые сообщения с сервера и отображает их
async def load_fresh_messages():
    global last_seen_id
    item = js.document.createElement("p")
    item.text = last_seen_id
    # 1. Загружать все сообщения каждую секунду (большой трафик)
    result = await u.fetch(f"/get_messages?after={last_seen_id}", method="GET")  # Делаем запрос
    # chat_window.innerHTML = ""  # Очищаем окно с сообщениями
    data = await result.json()
    all_messages = data["messages"]  # Берем список сообщений из ответа сервера
    all_users = data["users"]
    del_msgs = data["to_delete"]
    diff = find_new_users(all_users)
    for user in diff:
        users[user] = all_users[user]
    update_users(diff)
    for msg in all_messages:
        last_seen_id = msg["msg_id"]  # msg_id Последнего сообщение
        append_message(msg)
    for msg_id in del_msgs:
        delete_message(msg_id)
    u.set_timeout(1, load_fresh_messages) # Запускаем загрузку заново через секунду
    # 2. Загружать только новые сообщения


#append_message({"sender":"Елена Борисовна", "text":"Присылаем в чат только рабочие сообщения!!!", "time": "00:01"})
# Устнаваливаем действие при клике
send_message.onclick = send_message_click
send_login.onclick = send_login_click