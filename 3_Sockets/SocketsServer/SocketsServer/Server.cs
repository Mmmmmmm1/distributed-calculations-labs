﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections;

namespace Sockets
{
    public partial class frmMain : Form
    {
        private const int Port = 1010;                                                // порт, который будет указан при создании сокета
        private Socket ClientSock;                      // клиентский сокет
        private TcpListener Listener;                   // сокет сервера
        private UdpClient UdpListener = new UdpClient(Port);                  // сокет для подключения клиентов
        private List<Thread> Threads = new List<Thread>();      // список потоков приложения (кроме родительского)
        private bool _continue = true;                          // флаг, указывающий продолжается ли работа с сокетами
        private Dictionary<int, Thread> clientSocketsThreads = new Dictionary<int, Thread>();
        private Dictionary<int, Socket> clientSockets = new Dictionary<int, Socket>();
        private IPAddress IP = IPAddress.Any;

        // конструктор формы
        public frmMain()
        {
            InitializeComponent();

            IPHostEntry hostEntry = Dns.GetHostEntry(Dns.GetHostName());    // информация об IP-адресах и имени машины, на которой запущено приложение
            IP = hostEntry.AddressList[0];                        // IP-адрес, который будет указан при создании сокета

            // определяем IP-адрес машины в формате IPv4
            foreach (IPAddress address in hostEntry.AddressList)
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    IP = address;
                    break;
                }

            // вывод IP-адреса машины и номера порта в заголовок формы, чтобы можно было его использовать для ввода имени в форме клиента, запущенного на другом вычислительном узле
            this.Text += "     " + IP.ToString() + "  :  " + Port.ToString();

            // создаем серверный сокет (Listener для приема заявок от клиентских сокетов)
            Listener = new TcpListener(IP, Port);
            Listener.Start();

            // создаем и запускаем поток, выполняющий обслуживание серверного сокета
            Threads.Clear();
            Threads.Add(new Thread(ReceiveMessage));
            Threads[Threads.Count - 1].Start();
            Threads.Add(new Thread(RecieveConnectionRequest));
            Threads[Threads.Count - 1].Start();
        }

        private void RecieveConnectionRequest()
        {
            // входим в бесконечный цикл для работы с подключением клиентских сокетов
            while (_continue)
            {
                IPEndPoint clientIPtoConnect = new IPEndPoint(0, 0);
                UdpListener.Receive(ref clientIPtoConnect);
                byte[] buff = Encoding.Unicode.GetBytes("Connection accepted");
                UdpListener.Send(buff, buff.Length, clientIPtoConnect);
            }
        }

        // работа с клиентскими сокетами
        private void ReceiveMessage()
        {
            // входим в бесконечный цикл для работы с клиентскими сокетом
            while (_continue)
            {
                ClientSock = Listener.AcceptSocket();           // получаем ссылку на очередной клиентский сокет
                Threads.Add(new Thread(ReadMessages));          // создаем и запускаем поток, обслуживающий конкретный клиентский сокет
                int sockHash = ClientSock.GetHashCode();
                clientSockets[sockHash] = ClientSock;
                clientSocketsThreads[sockHash] = Threads[Threads.Count - 1];
                Threads[Threads.Count - 1].Start(sockHash);
            }
        }

        // получение сообщений от конкретного клиента
        private void ReadMessages(object sockHash)
        {
            int sockHashInt = (int)sockHash;
            string msg = "";        // полученное сообщение

            // входим в бесконечный цикл для работы с клиентским сокетом
            while (_continue)
            {
                byte[] buff = new byte[1024];                           // буфер прочитанных из сокета байтов
                try
                {
                    clientSockets[sockHashInt].Receive(buff);                     // получаем последовательность байтов из сокета в буфер buff
                    msg = System.Text.Encoding.Unicode.GetString(buff);     // выполняем преобразование байтов в последовательность символов

                    rtbMessages.Invoke((MethodInvoker)delegate
                    {
                        msg = msg.Replace("\0", "");
                        if (msg != "")
                        {
                            rtbMessages.Text += "\n >> " + msg;             // выводим полученное сообщение на форму
                            buff = Encoding.Unicode.GetBytes(msg);
                            foreach (int socketHash in clientSocketsThreads.Keys)
                            {
                                int bytesSent = (clientSockets[socketHash]).Send(buff, SocketFlags.None);
                            }
                        }
                    });
                }
                catch (Exception ex)
                {
                    Thread tt = clientSocketsThreads[sockHashInt];
                    clientSocketsThreads.Remove(sockHashInt);
                    clientSockets.Remove(sockHashInt);
                    tt.Abort();
                    break;
                }
                Thread.Sleep(500);
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            _continue = false;      // сообщаем, что работа с сокетами завершена

            // завершаем все потоки
            foreach (Thread t in Threads)
            {
                t.Abort();
                t.Join(500);
            }

            // закрываем клиентский сокет
            if (ClientSock != null)
                ClientSock.Close();

            // приостанавливаем "прослушивание" серверного сокета
            if (Listener != null)
                Listener.Stop();
            if (UdpListener != null)
                UdpListener.Close();
        }
    }
}