﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace Sockets
{
    public partial class frmMain : Form
    {
        private IPAddress IP;                           // IP-адрес клиента
        private UdpClient ConncetionClient = new UdpClient();     // клиентский сокет
        private TcpClient Client = new TcpClient();     // клиентский сокет
        private string login;
        private Thread t;
        private bool _continue = true;
        const int Port = 1010;// номер порта, через который выполняется обмен сообщениями

        // конструктор формы
        public frmMain()
        {
            InitializeComponent();
            ConncetionClient.EnableBroadcast = true;

            IPHostEntry hostEntry = Dns.GetHostEntry(Dns.GetHostName());    // информация об IP-адресах и имени машины, на которой запущено приложение
            IP = hostEntry.AddressList[0];                                  // IP-адрес, который будет указан в заголовке окна для идентификации клиента

            // определяем IP-адрес машины в формате IPv4
            foreach (IPAddress address in hostEntry.AddressList)
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    IP = address;
                    break;
                }
            this.Text += "     " + IP.ToString();                           // выводим IP-адрес текущей машины в заголовок формы
        }

        private void ReadMessage()
        {
            IPEndPoint from = new IPEndPoint(0, 0);
            string msg = "";
            while (_continue)
            {
                byte[] buff = new byte[1024];
                Stream stm = Client.GetStream();
                stm.Read(buff, 0, 1024);
                msg = Encoding.Unicode.GetString(buff);
                chat.Invoke((MethodInvoker)delegate
                {
                    if (msg.Replace("\0", "") != "")
                        chat.Text += "\n >> " + msg;             // выводим полученное сообщение на форму
                });
                Thread.Sleep(500);
            }
        }
        // подключение к серверному сокету
        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] buff = Encoding.Unicode.GetBytes("Connect request");
                ConncetionClient.Send(buff, buff.Length, "255.255.255.255", Port);

                IPEndPoint from = null;             
                buff = ConncetionClient.Receive(ref from);
                string msg = Encoding.Unicode.GetString(buff);
                chat.Invoke((MethodInvoker)delegate
                {
                    if (msg.Replace("\0", "") != "")
                        chat.Text += "\n >> " + msg;             // выводим полученное сообщение на форму
                });
                ConncetionClient.Close();

                Client.Connect(from.Address, Port);                       // подключение к серверному сокету
                btnConnect.Enabled = false;
                btnSend.Enabled = true;
                login = loginT.Text;
                t = new Thread(ReadMessage);
                t.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Не удалось подключиться к серверу!");
            }
        }

        // отправка сообщения
        private void btnSend_Click(object sender, EventArgs e)
        {
            byte[] buff = Encoding.Unicode.GetBytes(login + " >> " + tbMessage.Text);   // выполняем преобразование сообщения (вместе с идентификатором машины) в последовательность байт
            Stream stm = Client.GetStream();
            stm.Write(buff, 0, buff.Length);
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Client.Close();         // закрытие клиентского сокета
            _continue = false;
            if (t != null)
                t.Abort();
        }

        private void lblIP_Click(object sender, EventArgs e)
        {

        }

        private void tbIP_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (loginT.Text.Length == 0)
            {
                loginT.BackColor = Color.Pink;
                btnConnect.Enabled = false;
            }
            else
            {
                loginT.BackColor = Color.White;
                btnConnect.Enabled = true;
            }
        }
    }
}