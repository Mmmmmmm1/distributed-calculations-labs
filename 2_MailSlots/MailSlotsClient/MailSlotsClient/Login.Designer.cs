﻿namespace MailSlotsClient
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbMailSlot = new System.Windows.Forms.TextBox();
            this.lblMailSlot = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.loginText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbMailSlot
            // 
            this.tbMailSlot.Location = new System.Drawing.Point(106, 21);
            this.tbMailSlot.Name = "tbMailSlot";
            this.tbMailSlot.Size = new System.Drawing.Size(278, 20);
            this.tbMailSlot.TabIndex = 3;
            this.tbMailSlot.Text = "ServerMailslot";
            // 
            // lblMailSlot
            // 
            this.lblMailSlot.AutoSize = true;
            this.lblMailSlot.Location = new System.Drawing.Point(12, 15);
            this.lblMailSlot.Name = "lblMailSlot";
            this.lblMailSlot.Size = new System.Drawing.Size(88, 26);
            this.lblMailSlot.TabIndex = 4;
            this.lblMailSlot.Text = "Введите только\r\nимя мейлслота ";
            // 
            // btnConnect
            // 
            this.btnConnect.Enabled = false;
            this.btnConnect.Location = new System.Drawing.Point(12, 79);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(372, 26);
            this.btnConnect.TabIndex = 5;
            this.btnConnect.Text = "Подключиться";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(12, 47);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(72, 26);
            this.lblMessage.TabIndex = 7;
            this.lblMessage.Text = "Введите ваш\r\nлогин*";
            // 
            // loginText
            // 
            this.loginText.BackColor = System.Drawing.Color.Pink;
            this.loginText.Location = new System.Drawing.Point(106, 53);
            this.loginText.Name = "loginText";
            this.loginText.Size = new System.Drawing.Size(278, 20);
            this.loginText.TabIndex = 6;
            this.loginText.TextChanged += new System.EventHandler(this.loginText_TextChanged);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 113);
            this.ControlBox = false;
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.loginText);
            this.Controls.Add(this.tbMailSlot);
            this.Controls.Add(this.lblMailSlot);
            this.Controls.Add(this.btnConnect);
            this.Name = "Login";
            this.Text = "Login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Login_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblMailSlot;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label lblMessage;
        public System.Windows.Forms.TextBox tbMailSlot;
        public System.Windows.Forms.TextBox loginText;
    }
}