﻿using MailSlots;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MailSlotsClient
{
    public partial class Login : Form
    {
        private Int32 HandleMailSlot;
        public Login()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                // открываем мэйлслот, имя которого указано в поле tbMailSlot
                HandleMailSlot = DIS.Import.CreateFile("\\\\*\\mailslot\\" + tbMailSlot.Text, DIS.Types.EFileAccess.GenericWrite, DIS.Types.EFileShare.Read, 0, DIS.Types.ECreationDisposition.OpenExisting, 0, 0);
                if (HandleMailSlot != -1)
                {
                    DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                    MessageBox.Show("Не удалось подключиться к мейлслоту");
            }
            catch
            {
                MessageBox.Show("Не удалось подключиться к мейлслоту");
            }
        }



        private void loginText_TextChanged(object sender, EventArgs e)
        {
            if (loginText.Text.Length == 0)
            {
                loginText.BackColor = Color.Pink;
                btnConnect.Enabled = false;
            }
            else
            {
                loginText.BackColor = Color.White;
                btnConnect.Enabled = true;
            }

        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            DIS.Import.CloseHandle(HandleMailSlot);     // закрываем дескриптор мэйлслота
        }
    }
}
