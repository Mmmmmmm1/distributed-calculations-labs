﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace WaveAlg
{
    public class Topology : IDisposable
    {
        public Node CoreNode { get; set; }
        public List<Node> Nodes { get; set; }
        Dictionary<string, int> loads = new Dictionary<string, int>();
        public Topology(Node Core, List<Node> InNodes)
        {
            CoreNode = Core;
            Nodes = InNodes;
            CoreNode.InitiateQueueListenening();
            foreach (Node node in Nodes)
            {
                node.InitiateQueueListenening();
            }
        }
        ~Topology()
        {
            Dispose();
        }
        public List<NodeConnection> GetAllNodeConnections()
        {
            List<NodeConnection> result = new List<NodeConnection>();
            List<Node> nodes = new List<Node>
            {
                CoreNode
            };
            nodes.AddRange(Nodes);
            foreach (Node node in nodes)
            {
                foreach (NodeConnection conNode in node.OutNodesWithConnections.Values)
                {
                    result.Add(conNode);
                }
            }
            return result;
        }

        public void GetNodesLoadFinn()
        {
            foreach (var n in Nodes)
            {
                n.StopQueueListenening();
                foreach (NodeConnection ngh in n.OutNodesWithConnections.Values)
                {
                    ngh.ClearQueue();
                }
            }
            CoreNode.StopQueueListenening();
            foreach (NodeConnection ngh in CoreNode.OutNodesWithConnections.Values)
            {
                ngh.ClearQueue();
            }
            CoreNode.ResetIncNinc();
            foreach (var node in Nodes)
            {
                node.ResetIncNinc();
            }


            Message sendMsg = new Message(new List<List<string>>() { CoreNode.inc, CoreNode.ninc })
            {
                Label = Node.SEND_LOAD_MSG
            };
            Console.WriteLine($"{CoreNode.Ip}: Sending inc & ninc to neighbours...");
            foreach (NodeConnection ngh in CoreNode.OutNodesWithConnections.Values)
            {
                ngh.SendMessage(sendMsg);
            }
            CoreNode.state = State.used;
            Console.WriteLine($"{CoreNode.Ip}: Is used ");
            while (!Node.isFinnFinished) { }

            Console.WriteLine($"\nLoads found:");
            Dictionary<string, int> loads = new Dictionary<string, int>();
            foreach (string load in CoreNode.inc)
            {
                string[] info = load.Split(new char[] { '_' });
                loads[info[0]] = int.Parse(info[1]);
                Console.WriteLine($"{info[0]}: {info[1]}");
            }
            this.loads = loads;
        }
        public void OptimizeNodes()
        {
            Console.WriteLine("Optimizing...");
            Dictionary<string, int> overLoadedNodes = loads.Where(a => a.Value > TreeNode.NODE_LOAD_CAPACITY).ToDictionary(a => a.Key, a => a.Value);
            var overLoadedNodesSorted = (from entry in overLoadedNodes orderby entry.Value descending select entry).ToDictionary(a => a.Key, a => a.Value);
            Dictionary<string, int> lessLoadedNodes = loads.Where(a => a.Value <= TreeNode.NODE_LOAD_CAPACITY).ToDictionary(a => a.Key, a => a.Value);
            var lessLoadedNodesSorted = (from entry in lessLoadedNodes orderby entry.Value ascending select entry).ToDictionary(a => a.Key, a => a.Value);

            var ips = overLoadedNodesSorted.Keys.ToList();
            foreach (var ip in ips)
            {
                Console.WriteLine($"{ip} is overloaded. Trying to balance...");
                string mostFreeNode = lessLoadedNodesSorted.First().Key;
                int excessLoad = overLoadedNodesSorted[ip] - TreeNode.NODE_LOAD_CAPACITY;

                if (lessLoadedNodesSorted[mostFreeNode] + excessLoad <= TreeNode.NODE_LOAD_CAPACITY)
                {
                    Console.WriteLine($"Moving {excessLoad} load to {mostFreeNode}...");
                    lessLoadedNodesSorted[mostFreeNode] += excessLoad;
                    overLoadedNodesSorted[ip] -= excessLoad;
                    Console.WriteLine($"{ip}'s updated load: {overLoadedNodesSorted[ip]}.\n{mostFreeNode} updated load: {lessLoadedNodesSorted[mostFreeNode]}\n");
                    lessLoadedNodesSorted = (from entry in lessLoadedNodesSorted orderby entry.Value ascending select entry).ToDictionary(a => a.Key, a => a.Value);
                }
                else
                {
                    Console.WriteLine($"Unable to balance {ip}.\n");
                }
            }
            Console.WriteLine("Performing desired balancing...");
            UpdateNodes(overLoadedNodesSorted);
            UpdateNodes(lessLoadedNodesSorted);
        }
        private void UpdateNodes(Dictionary<string, int> balancedNodes)
        {
            if (balancedNodes.ContainsKey(CoreNode.Ip))
            {
                CoreNode.currentNodeLoad = balancedNodes[CoreNode.Ip];
                Console.WriteLine($"{CoreNode.Ip} updated!");
            }
            foreach (var node in Nodes)
            {
                if (balancedNodes.ContainsKey(node.Ip))
                {
                    node.currentNodeLoad = balancedNodes[node.Ip];
                    Console.WriteLine($"{node.Ip} updated!");
                }
            }
        }
        public void PrintNodePaths()
        {
            var allNodes = new List<Node>() { CoreNode };
            allNodes.AddRange(Nodes);
            foreach (var rowNode in allNodes)
            {
                string row = "|";
                foreach (var node in allNodes)
                {
                    if (rowNode.OutNodesWithConnections.ContainsKey(node))
                    {
                        row += " 1";
                        continue;
                    }
                    row += " 0";
                }
                row += " |";
                Console.WriteLine(row);
            }
        }
        public static Topology GenerateExampleTopolgy()
        {
            Random r = new Random();
            int count = 4;
            string baseIp = "192.168.0.0.";
            List<Node> nodes = new List<Node>(count);
            int[] nLoad = new int[4] { 4,6,15,9};
            for (int i = 0; i < count; i++)
            {
                //int nLoad = r.Next(1, 15);
                nodes.Add(new Node(baseIp + i, nLoad[i]));
            }
            int connCount = 6;
            NodeConnection[] connections = new NodeConnection[connCount];
            for (int i = 0; i < connCount; i++)
            {
                connections[i] = new NodeConnection(MessageQueue.Create(".\\Private$\\" + i));
            }

            nodes[0].InNodes = new Dictionary<Node, NodeConnection>() {
                {nodes[1], connections[3]},
                {nodes[2], connections[5]}
            };
            nodes[0].OutNodesWithConnections = new Dictionary<Node, NodeConnection>() {
                {nodes[3], connections[0]}
            };

            nodes[1].InNodes = new Dictionary<Node, NodeConnection>() {
                {nodes[2], connections[4]}
            };
            nodes[1].OutNodesWithConnections = new Dictionary<Node, NodeConnection>() {
                {nodes[0], connections[3]},
                {nodes[3], connections[1]}
            };

            nodes[2].InNodes = new Dictionary<Node, NodeConnection>() {
                {nodes[3], connections[2]}
            };
            nodes[2].OutNodesWithConnections = new Dictionary<Node, NodeConnection>() {
                {nodes[0], connections[5]},
                {nodes[1], connections[4]}
            };

            nodes[3].InNodes = new Dictionary<Node, NodeConnection>() {
                {nodes[0], connections[0]},
                {nodes[1], connections[1]}
            };
            nodes[3].OutNodesWithConnections = new Dictionary<Node, NodeConnection>() {
                {nodes[2], connections[2]}
            };
            //nodes[0].InNodes = new Dictionary<Node, NodeConnection>() {
            //    {nodes[1], connections[0]}
            //};
            //nodes[0].OutNodesWithConnections = new Dictionary<Node, NodeConnection>() {
            //    {nodes[2], connections[1]},
            //    {nodes[4], connections[4]}
            //};

            //nodes[1].InNodes = new Dictionary<Node, NodeConnection>() {
            //    {nodes[4], connections[3]}
            //};
            //nodes[1].OutNodesWithConnections = new Dictionary<Node, NodeConnection>() {
            //    {nodes[0], connections[0]},
            //    {nodes[3], connections[2]}
            //};

            //nodes[2].InNodes = new Dictionary<Node, NodeConnection>() {
            //    {nodes[0], connections[1]}
            //};
            //nodes[2].OutNodesWithConnections = new Dictionary<Node, NodeConnection>() {
            //    {nodes[5], connections[5]}
            //};

            //nodes[3].InNodes = new Dictionary<Node, NodeConnection>() {
            //    {nodes[1], connections[2]}
            //};
            //nodes[3].OutNodesWithConnections = new Dictionary<Node, NodeConnection>() {
            //    {nodes[4], connections[7]},
            //    {nodes[5], connections[6]}
            //};
            //nodes[4].InNodes = new Dictionary<Node, NodeConnection>() {
            //    {nodes[0], connections[4]},
            //    {nodes[3], connections[7]},
            //    {nodes[5], connections[8]}
            //};
            //nodes[4].OutNodesWithConnections = new Dictionary<Node, NodeConnection>() {
            //    {nodes[1], connections[3]}
            //};
            //nodes[5].InNodes = new Dictionary<Node, NodeConnection>() {
            //    {nodes[2], connections[5]},
            //    {nodes[3], connections[6]}
            //};
            //nodes[5].OutNodesWithConnections = new Dictionary<Node, NodeConnection>() {
            //    {nodes[4], connections[8]}
            //};

            Node core = nodes[0];
            nodes.Remove(core);
            core.isCore = true;
            return new Topology(core, nodes);
        }

        public void Dispose()
        {
            CoreNode.Dispose();
            foreach (Node node in Nodes)
            {
                node.Dispose();
            }
            GC.SuppressFinalize(this);
        }
    }
}
