﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveAlg
{
    public class TaskCommunication
    {
        public int n {  get; set; }
        public int b {  get; set; }
        public TaskCommunication(int n, int b)
        {
            this.n = n;
            this.b = b;
        }
    }
}
