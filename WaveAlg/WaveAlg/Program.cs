﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaveAlg
{
    internal class Program
    {
        static void PrintMenu()
        {
            Console.WriteLine("\n\nВолновые алгоритмы. Выберете действие:\n 1)Древовидная топология (Эхо)\n 2)Произвольная топология (Финна)\n q)Выход");
        }

        static void Menu()
        {
            PrintMenu();
            bool quit = false;
            do
            {
                string input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        {
                            using (TreeTopology topology = TreeTopology.GenerateExampleTreeTopolgy())
                            {
                                Console.WriteLine("Tree topology:" );
                                topology.CoreNode.PrintTreeFromThis("", true);

                                Console.WriteLine("\nGetting nodes load with echo: ");
                                topology.GetNodesLoadEcho();

                                Console.WriteLine("\nPress Enter to balance... ");
                                Console.ReadLine();

                                Console.WriteLine("\nBalancing loads: ");
                                topology.OptimizeNodes();

                                Console.WriteLine("\nPress Enter to get updated load... ");
                                Console.ReadLine();

                                Console.WriteLine("\nNew nodes load: ");
                                topology.GetNodesLoadEcho();
                                Thread.Sleep(2000);
                            }
                            PrintMenu();
                            break;
                        }
                    case "2":
                        {
                            using (Topology topology = Topology.GenerateExampleTopolgy())
                            {
                                Console.WriteLine("Tree topology:" );
                                topology.PrintNodePaths();

                                Console.WriteLine("\nGetting nodes load with finn: ");
                                topology.GetNodesLoadFinn();

                                Console.WriteLine("\nPress Enter to balance... ");
                                Console.ReadLine();

                                Console.WriteLine("\nBalancing loads: ");
                                topology.OptimizeNodes();

                                Console.WriteLine("\nPress Enter to get updated load... ");
                                Console.ReadLine();

                                Console.WriteLine("\nNew nodes load: ");
                                topology.GetNodesLoadFinn();
                                Thread.Sleep(2000);
                            }
                            PrintMenu();
                            break;
                        }
                    case "q":
                        {
                            quit = true;
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Введите корректное значение!");
                            break;
                        }
                }
            } while (!quit);
        }

        static void Main(string[] args)
        {
            Menu();
        }
    }
}
