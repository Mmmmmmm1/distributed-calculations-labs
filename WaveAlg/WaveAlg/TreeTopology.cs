﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WaveAlg
{
    internal class TreeTopology : IDisposable
    {
        public TreeNode CoreNode { get; set; }
        List<TreeNode> Nodes { get; set; }
        Dictionary<string, int> loads = new Dictionary<string, int>();
        public TreeTopology(TreeNode Core, List<TreeNode> InNodes)
        {
            CoreNode = Core;
            Nodes = InNodes;
            CoreNode.InitiateQueueListenening();
            foreach (TreeNode node in Nodes)
            {
                node.InitiateQueueListenening();
            }
        }
        ~TreeTopology()
        {
            Dispose();
        }
        public void GetNodesLoadEcho()
        {
            CoreNode.KnownLoads = new List<string>();
            Message initiateEchoMsg = new Message("Echoing...") { Label = TreeNode.ECHO_MSG };
            Console.WriteLine($"{CoreNode.Ip}: Sending to children...");
            foreach (NodeConnection child in CoreNode.OutNodesWithConnections.Values)
            {
                child.SendMessage(initiateEchoMsg);
            }
            CoreNode.GetChildrenEcho();
            Console.WriteLine($"\nLoads found:");
            Dictionary<string, int> loads = new Dictionary<string, int>();
            foreach (string load in CoreNode.KnownLoads)
            {
                string[] info = load.Split(new char[] { '_' });
                loads[info[0]] = int.Parse(info[1]);
                Console.WriteLine($"{info[0]}: {info[1]}");
            }
            this.loads = loads;
        }
        public void OptimizeNodes()
        {
            Console.WriteLine("Optimizing...");
            Dictionary<string, int> overLoadedNodes = loads.Where(a => a.Value > TreeNode.NODE_LOAD_CAPACITY).ToDictionary(a => a.Key, a => a.Value);
            var overLoadedNodesSorted = (from entry in overLoadedNodes orderby entry.Value descending select entry).ToDictionary(a => a.Key, a => a.Value);
            Dictionary<string, int> lessLoadedNodes = loads.Where(a => a.Value <= TreeNode.NODE_LOAD_CAPACITY).ToDictionary(a => a.Key, a => a.Value);
            var lessLoadedNodesSorted = (from entry in lessLoadedNodes orderby entry.Value ascending select entry).ToDictionary(a => a.Key, a => a.Value);

            var ips = overLoadedNodesSorted.Keys.ToList();
            foreach (var ip in ips)
            {
                Console.WriteLine($"{ip} is overloaded. Trying to balance...");
                string mostFreeNode = lessLoadedNodesSorted.First().Key;
                int excessLoad = overLoadedNodesSorted[ip] - TreeNode.NODE_LOAD_CAPACITY;
                if (lessLoadedNodesSorted[mostFreeNode] + excessLoad <= TreeNode.NODE_LOAD_CAPACITY)
                {
                    Console.WriteLine($"Moving {excessLoad} load to {mostFreeNode}...");
                    lessLoadedNodesSorted[mostFreeNode] += excessLoad;
                    overLoadedNodesSorted[ip] -= excessLoad;
                    Console.WriteLine($"{ip}'s updated load: {overLoadedNodesSorted[ip]}.\n{mostFreeNode} updated load: {lessLoadedNodesSorted[mostFreeNode]}\n");
                    lessLoadedNodesSorted = (from entry in lessLoadedNodesSorted orderby entry.Value ascending select entry).ToDictionary(a => a.Key, a => a.Value);
                }
                else
                {
                    Console.WriteLine($"Unable to balance {ip}.\n");
                }
            }
            Console.WriteLine("Performing desired balancing...");
            UpdateNodes(overLoadedNodesSorted);
            UpdateNodes(lessLoadedNodesSorted);
        }
        private void UpdateNodes(Dictionary<string, int> balancedNodes)
        {
            if (balancedNodes.ContainsKey(CoreNode.Ip))
            {
                CoreNode.currentNodeLoad = balancedNodes[CoreNode.Ip];
                Console.WriteLine($"{CoreNode.Ip} updated!");
            }
            foreach (var node in Nodes)
            {
                if (balancedNodes.ContainsKey(node.Ip))
                {
                    node.currentNodeLoad = balancedNodes[node.Ip];
                    Console.WriteLine($"{node.Ip} updated!");
                }
            }
        }
        public static TreeTopology GenerateExampleTreeTopolgy()
        {
            Random r = new Random();
            int count = 8;
            string baseIp = "192.168.0.0.";
            List<TreeNode> nodes = new List<TreeNode>(count);
            for (int i = 0; i < count; i++)
            {
                int nLoad = r.Next(1, 15);
                nodes.Add(new TreeNode(baseIp + i, nLoad));
            }
            NodeConnection[] connections = new NodeConnection[count - 1];
            for (int i = 0; i < count - 1; i++)
            {
                connections[i] = new NodeConnection(MessageQueue.Create(".\\Private$\\" + i));
            }
            nodes[0].OutNodesWithConnections = new Dictionary<TreeNode, NodeConnection>() {
                {nodes[1], connections[0]},
                {nodes[2], connections[1]},
                {nodes[3], connections[2]}
            };

            nodes[1].ParentWithConnection = new Dictionary<TreeNode, NodeConnection> { { nodes[0], connections[0] } };
            nodes[1].OutNodesWithConnections = new Dictionary<TreeNode, NodeConnection>() {
                {nodes[4], connections[3]},
                {nodes[5], connections[4]},
                {nodes[6], connections[5]}
            };

            nodes[2].ParentWithConnection = new Dictionary<TreeNode, NodeConnection> { { nodes[0], connections[1] } };
            nodes[2].OutNodesWithConnections = new Dictionary<TreeNode, NodeConnection>() {
                {nodes[7], connections[6]}
            };

            nodes[3].ParentWithConnection = new Dictionary<TreeNode, NodeConnection> { { nodes[0], connections[2] } };
            nodes[4].ParentWithConnection = new Dictionary<TreeNode, NodeConnection> { { nodes[1], connections[3] } };
            nodes[5].ParentWithConnection = new Dictionary<TreeNode, NodeConnection> { { nodes[1], connections[4] } };
            nodes[6].ParentWithConnection = new Dictionary<TreeNode, NodeConnection> { { nodes[1], connections[5] } };
            nodes[7].ParentWithConnection = new Dictionary<TreeNode, NodeConnection> { { nodes[2], connections[6] } };

            TreeNode core = nodes[0];
            nodes.Remove(core);

            return new TreeTopology(core, nodes);
        }

        public void Dispose()
        {
            CoreNode.Dispose();
            foreach (TreeNode node in Nodes)
            {
                node.Dispose();
            }
            GC.SuppressFinalize(this);
        }
    }
}
