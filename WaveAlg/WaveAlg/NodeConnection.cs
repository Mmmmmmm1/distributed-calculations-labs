﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Messaging;
using System.Threading;

namespace WaveAlg
{
    public class NodeConnection
    {
        public MessageQueue Q { get; set; }
        public NodeConnection(MessageQueue q)
        {
            Q = q;
        }
        public Message RecieveMessage(string msgType)
        {
            Thread.Sleep(1000);
            if (Q != null && MessageQueue.Exists(Q.Path))
            {
                try
                {
                    if (MessageQueue.Exists(Q.Path))
                    {
                        if (Q.Peek() != null && Q.Peek().Label == msgType)
                        {
                            Message msg = Q.Receive(TimeSpan.FromSeconds(1.0));
                            return msg;
                        }
                    }
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }
        public void SendMessage(Message message)
        {
            if (Q != null && MessageQueue.Exists(Q.Path))
            {
                Q.Send(message.Body, message.Label);
            }
        }
        public void ClearQueue()
        {
            if (Q != null && MessageQueue.Exists(Q.Path))
            {
                Q.Purge();
            }
        }
    }
}
