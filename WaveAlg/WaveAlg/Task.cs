﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveAlg
{
    public class Task
    {
        public Func<int> Complexity { get; }
        public Dictionary<Task, TaskCommunication> Communications {  get; set; }
        public Task(Func<int> complexity, Dictionary<Task, TaskCommunication> communications)
        {
            this.Complexity = complexity;
            this.Communications = communications;
        }
    }
}
