﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaveAlg
{
    public enum State { active, virgin, used }
    public class Node : IDisposable
    {
        public static string SEND_LOAD_MSG = "Sending processors load";
        public static int NODE_LOAD_CAPACITY = 10;
        public string Ip { get; set; }
        public Dictionary<Node, NodeConnection> InNodes { get; set; } = new Dictionary<Node, NodeConnection> { };
        public Dictionary<Node, NodeConnection> OutNodesWithConnections { get; set; } = new Dictionary<Node, NodeConnection>();
        public List<string> inc { get; set; } = new List<string>();
        public List<string> ninc { get; set; } = new List<string>();
        private Thread cT;
        private bool _continue = true;
        public bool isCore = false;
        public static bool isFinnFinished = false;
        public State state = State.virgin;

        public int currentNodeLoad = 0;
        public Node(string ip, Dictionary<Node, NodeConnection> iNodes, Dictionary<Node, NodeConnection> oNodes, int load)
        {
            Ip = ip;
            OutNodesWithConnections = oNodes;
            InNodes = iNodes;
            currentNodeLoad = load;
            inc.Add($"{Ip}_{load}");
        }
        public Node(string ip, int load)
        {
            Ip = ip;
            currentNodeLoad = load;
            inc.Add($"{Ip}_{load}");
        }

        ~Node()
        {
            Dispose();
        }
        public void Dispose()
        {
            _continue = false;
            if (cT != null)
            {
                cT.Abort();
            }
            foreach (NodeConnection conNode in OutNodesWithConnections.Values)
            {
                MessageQueue.Delete(conNode.Q.Path);
                conNode.Q = null;
            }
            GC.SuppressFinalize(this);
        }
        // must call after assigning all parents and children
        public void InitiateQueueListenening()
        {
            cT = new Thread(GetFinnMsg);
            cT.Start();
        }
        public void StopQueueListenening()
        {
            if (cT != null)
            {
                cT.Abort();
            }
        }
        public void ResetIncNinc()
        {
            state = State.virgin;
            InitiateQueueListenening();
            inc = new List<string>() { $"{Ip}_{currentNodeLoad}" };
            ninc = new List<string>();
            isFinnFinished = false;
        }
        public void GetFinnMsg()
        {
            while (_continue && !isFinnFinished)
            {
                foreach (Node node in InNodes.Keys)
                {
                    NodeConnection queue = InNodes[node];
                    Message msg = queue.RecieveMessage(SEND_LOAD_MSG);
                    if (msg != null && !isFinnFinished)
                    {
                        Console.WriteLine($"{Ip}: Recieved incoming msg from {node.Ip}... ");
                        List<List<string>> relevantMsg = msg.Body as List<List<string>>;
                        state = State.active;
                        Console.WriteLine($"{Ip}: Is active ");

                        bool isIncNincChanged = false;
                        foreach (var entry in relevantMsg[0])
                        {
                            if (!inc.Contains(entry) && !isFinnFinished)
                            {
                                inc.Add(entry);
                                isIncNincChanged = true;
                                Console.WriteLine($"{Ip}: Added {entry} to node's inc ");
                            }
                        }

                        foreach (var entry in relevantMsg[1])
                        {
                            if (!ninc.Contains(entry) && !isFinnFinished)
                            {
                                ninc.Add(entry);
                                isIncNincChanged = true;
                                Console.WriteLine($"{Ip}: Added {entry} to node's ninc ");
                            }
                        }
                        if (!InNodes.Where(e => e.Key.state == State.virgin).Any() && !ninc.Contains($"{Ip}_{currentNodeLoad}") && !isFinnFinished)
                        {
                            ninc.Add($"{Ip}_{currentNodeLoad}");
                            isIncNincChanged = true;
                            Console.WriteLine($"{Ip}: Added current node load to node's ninc ");
                        }

                        if (inc.Intersect(ninc).Count() == inc.Count && isCore)
                        {
                            isFinnFinished = true;
                            Console.WriteLine($"{Ip}: Core's inc and ninc are equal. Algorythm finished.");
                        }
                        if (!isFinnFinished && isIncNincChanged)
                        {
                            Message sendMsg = new Message(new List<List<string>>() { inc, ninc })
                            {
                                Label = SEND_LOAD_MSG
                            };
                            Console.WriteLine($"{Ip}: Sending message to node's neighbours... ");
                            foreach (var outN in OutNodesWithConnections.Keys)
                            {
                                OutNodesWithConnections[outN].SendMessage(sendMsg);
                            }
                            state = State.used;
                            Console.WriteLine($"{Ip}: Is used ");
                        }
                    }
                }
                Thread.Sleep(2000);
            }
        }
    }
}
