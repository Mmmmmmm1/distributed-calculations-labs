﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Threading;
using System.Xml.Linq;

namespace WaveAlg
{
    public class TreeNode : IDisposable
    {
        public static string ECHO_MSG = "Getting processors load";
        public static string SEND_LOAD_MSG = "Sending processors load";
        public static int NODE_LOAD_CAPACITY = 10;
        internal string Ip { get; }
        internal Dictionary<TreeNode, NodeConnection> ParentWithConnection { get; set; } = new Dictionary<TreeNode, NodeConnection>();
        internal Dictionary<TreeNode, NodeConnection> OutNodesWithConnections { get; set; } = new Dictionary<TreeNode, NodeConnection>();
        public List<string> KnownLoads { get; set; } = new List<string>();
        private Thread pT;
        private Thread cT;
        private bool _continue = true;

        public int currentNodeLoad = 0;

        public TreeNode(string ip, Dictionary<TreeNode, NodeConnection> oNodes, Dictionary<TreeNode, NodeConnection> parent, int load)
        {
            Ip = ip;
            OutNodesWithConnections = oNodes;
            ParentWithConnection = parent;
            currentNodeLoad = load;
        }
        public TreeNode(string ip, int load)
        {
            Ip = ip;
            currentNodeLoad = load;
        }
        ~TreeNode()
        {
            Dispose();
        }
        public void Dispose()
        {
            _continue = false;
            if (pT != null)
            {
                pT.Abort();
            }
            if (cT != null)
            {
                cT.Abort();
            }
            foreach (NodeConnection conNode in OutNodesWithConnections.Values)
            {
                MessageQueue.Delete(conNode.Q.Path);
                conNode.Q = null;
            }
            GC.SuppressFinalize(this);
        }
        // must call after assigning all parents and children
        public void InitiateQueueListenening()
        {
            if (ParentWithConnection.Count > 0)
            {
                pT = new Thread(GetParentEcho);
                pT.Start();
                if (OutNodesWithConnections.Count > 0)
                {
                    cT = new Thread(GetChildrenEcho);
                    cT.Start();
                }
            }
        }
        private void GetParentEcho()
        {
            NodeConnection queue = ParentWithConnection.First().Value;
            // pass to children or if none pass load list to parent
            while (_continue)
            {
                Message msg = queue.RecieveMessage(ECHO_MSG);
                if (msg != null)
                {
                    Console.WriteLine($"{Ip}: Recieved parent echo... ");
                    if (OutNodesWithConnections.Count == 0)
                    {
                        Console.WriteLine($"{Ip}: No children, sending load to parent...");
                        Message echo = new Message(new List<string>() { $"{Ip}_{currentNodeLoad}"})
                        {
                            Label = SEND_LOAD_MSG
                        };
                        queue.SendMessage(echo);
                    }
                    else
                    {
                        Console.WriteLine($"{Ip}: Sending to children...");
                        foreach (NodeConnection child in OutNodesWithConnections.Values)
                        {
                            child.SendMessage(msg);
                        }
                    }
                }

                Thread.Sleep(2000);
            }
        }
        public void GetChildrenEcho()
        {
            // get load list from msg from every child
            // pass to parent
            // reset
            List<string> children_ips = new List<string>();
            KnownLoads = new List<string>();
            while (_continue)
            {
                foreach (TreeNode node in OutNodesWithConnections.Keys)
                {
                    NodeConnection queue = OutNodesWithConnections[node];
                    Message msg = queue.RecieveMessage(SEND_LOAD_MSG);
                    if (msg != null && !children_ips.Contains(node.Ip))
                    {
                        Console.WriteLine($"{Ip}: Recieved child {node.Ip}'s echo... ");
                        children_ips.Add(node.Ip);
                        List<string> recievedLoads = msg.Body as List<string>;
                        KnownLoads.AddRange(recievedLoads);

                        // if all children sent their load
                        if (children_ips.Count == OutNodesWithConnections.Count)
                        {
                            KnownLoads.Add($"{Ip}_{currentNodeLoad}"); // sending this node load and it's children
                            // if this node is initiator end echos
                            if (ParentWithConnection.Count == 0)
                            {
                                Console.WriteLine($"{Ip}: Reached initiator.");
                                children_ips = new List<string>();
                                return;
                            }
                            Console.WriteLine($"{Ip}: Got all childrens' echoes. Sending loads to parent...");
                            // else send loads to parent
                            NodeConnection parentQueue = ParentWithConnection.First().Value;
                            Message loadMsg = new Message(KnownLoads)
                            {
                                Label = SEND_LOAD_MSG
                            };
                            parentQueue.SendMessage(loadMsg);
                            // resetting
                            children_ips = new List<string>();
                            KnownLoads = new List<string>();

                        }
                    }
                }
                Thread.Sleep(2000);
            }
        }
        public void PrintTreeFromThis(string indent, bool last)
        {
            Console.Write(indent);
            if (last)
            {
                Console.Write("\\-");
                indent += "  ";
            }
            else
            {
                Console.Write("|-");
                indent += "| ";
            }
            Console.WriteLine(Ip);

            for (int i = 0; i < OutNodesWithConnections.Count; i++)
                OutNodesWithConnections.Keys.ToList()[i].PrintTreeFromThis(indent, i == OutNodesWithConnections.Count - 1);
        }
    }
}
